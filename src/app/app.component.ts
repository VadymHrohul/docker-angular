import { Component, OnInit } from '@angular/core';
import { DataService } from './main-page-module/shared/services/data.service';
import { AllData } from './main-page-module/shared/models/data.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  allData: AllData;
  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    this.dataService.getData()
      .subscribe((response: AllData) => {
        this.allData = response;
      });
  }
}
