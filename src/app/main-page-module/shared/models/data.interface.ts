export interface AllData {
  data: DataItem[];
  bannerData: BannerData;
}

export interface DataItem {
  id: number;
  title: string;
  subtitle: string;
  imgUrl: string;
}

export interface BannerData extends Omit<DataItem, 'id'> {}
