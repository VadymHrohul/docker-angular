import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';
import { AllData } from '../models/data.interface';


@Injectable({ providedIn: 'root' })

export class DataService {
  constructor(private http: HttpClient) {}

  getData(): Observable<AllData> {
    return this.http.get<AllData>(environment.dbUrl);
  }
}
