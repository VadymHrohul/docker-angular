import { Component, Input, OnInit } from '@angular/core';
import { DataItem } from '../../shared/models/data.interface';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() dataItem: DataItem;

  constructor() { }

  ngOnInit(): void {
  }

}
