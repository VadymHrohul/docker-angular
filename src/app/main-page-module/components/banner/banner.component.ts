import { Component, Input, OnInit } from '@angular/core';
import { BannerData } from '../../shared/models/data.interface';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {
  @Input() bannerData: BannerData;

  constructor() { }

  ngOnInit(): void {

  }

}
